import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../userContext";

const ViewCourse = () => {
  /*fetch the details of our course by its id.*/

  //useParams from react-router-rom will allow us to retrieve the data from our Browser URL.
  //useParams() will return an object containing URL params.
  //console.log(useParams());
  //destructure the object from useParams() and save it in a variable:
  const { courseId } = useParams();

  //console.log(courseId);

  //get the global user state from our context:
  const { user } = useContext(UserContext);
  console.log(user);

  //state to save our course details.
  const [courseDetails, setCourseDetails] = useState({
    name: null,
    description: null,
    price: null,
  });

  //useEffect to get course details
  useEffect(() => {
    //fetch to get our course details:
    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        //console.log(data);
        setCourseDetails({
          name: data.name,
          description: data.description,
          price: data.price,
        });
      });
  }, [courseId]);

  //enroll function will be run when logged in user clicks on the enroll button
  function enroll() {
    console.log("enroll");
    console.log(courseId);

    //fetch for enroll:
    //needs the courseId to be passed in the body.
    //to pass the token as part of the Authorization.
    //add fetch for enroll
    fetch("http://localhost:4000/users/enroll", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        courseId: courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message === "Enrolled Successfully.") {
          Swal.fire({
            icon: "success",
            title: "Enrolled Successully.",
            text: "Thank you for enrolling.",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Enrollment Failed.",
            text: data.message,
          });
        }
      });
  }

  return (
    <Row className='mt-5'>
      <Col>
        <Card>
          <Card.Body className='text-center'>
            <Card.Title>{courseDetails.name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{courseDetails.description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{courseDetails.price}</Card.Text>
          </Card.Body>
          {user.id && user.isAdmin === false ? (
            <Button variant='primary' className='btn-block' onClick={enroll}>
              Enroll
            </Button>
          ) : (
            <Link className='btn btn-danger btn-block' to='/login'>
              Login To Enroll
            </Link>
          )}
        </Card>
      </Col>
    </Row>
  );
};
export default ViewCourse;
